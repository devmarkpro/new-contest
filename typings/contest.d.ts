import { ObjectID } from 'mongodb';

export interface MetaContestDTO {
  _id?: ObjectID;
  appId: string;
  title: string;
  prize: number;
  beginTime: Date;
  endTime?: Date;
  duration: number;
  neededCorrectors: number;
  allowedCorrectorUsageTimes: number;
  allowCorrectTilQuestion: number;
  neededTickets: number;
}
declare interface AttachAnswerRequestDTO {
  _id?: ObjectID;
  order: number;
  text: string;
  hit: number;
}
declare interface AttachQuestionRequestDTO {
  _id?: ObjectID;
  order: number;
  text: string;
  options: AttachAnswerRequestDTO[];
}
declare interface AttachRequestDTO {
  _id?: ObjectID;
  metaId: string;
  questions: AttachQuestionRequestDTO[];
  correctAnswers: number[];
}
declare interface UpdateContestDTO {
  meta: MetaContestDTO;
  questions: AttachQuestionRequestDTO[];
  correctAnswers: number[];
}
