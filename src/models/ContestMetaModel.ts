import { Schema } from 'mongoose';

export interface ContestMetaModel {
  appId: string;
  title: string;
  prize: number;
  beginTime: Date;
  duration: number;
  itemDuration: number;
  neededCorrectors: number;
  allowedCorrectorUsageTimes: number;
  allowCorrectTilQuestion: number;
  neededTickets: number;
  deletedAt?: Date;
  deleted?: boolean;
  questions: [
    {
      _id: string;
      order: number;
      text: string;
      options: [
        {
          _id: string;
          order: number;
          text: string;
        }
      ];
    }
  ];
  correctAnswers: number[];
}
