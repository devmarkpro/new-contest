export interface UserTrackModel {
  userId: string;
  contestId: string;
  questionIndex: number;
  questionId: string;
  answerId: string;
  lastAnsweredIndex: number;
  lastFetchTime: Date;
  canPlay: boolean;
  challengeStatus: string;
  correctors: number;
  correctorUsedTimes: number;
}
