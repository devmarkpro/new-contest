export class AppError extends Error {
  constructor(msg: string) {
    super(msg);
    this.message = msg;
    this.name = 'USER_SERVICE';
  }
}
