import BrokerInstance from '../services/Broker';
import { ContestModel } from '../schemas';
import * as Logger from '../utils/Logger';
import moment from 'moment-timezone';

export default async (job: any) => {
  Logger.debug(
    `Processing Contest job: ${job.data.contestId} at ${moment()
      .tz('Asia/Tehran')
      .toString()}`,
  );
  const broker = BrokerInstance();
  const contest = await ContestModel.findById(job.data.contestId);
  if (!contest) {
    throw new Error(`Cannot found any contest : ${job.data.contestId}`);
    return Promise.reject(
      new Error('Cannot find contest : ' + job.data.contestId),
    );
  }
  const question = contest.questions.find(a => a.order === job.data.order);
  if (!question) {
    throw new Error('No question found for contest : ' + job.data.contestId);
    return Promise.reject(
      new Error('No question found for contest : ' + job.data.contestId),
    );
  }
  const payload = {
    id: question._id,
    text: question.text,
    options: [],
  };

  question.options.forEach(o => {
    // @ts-ignore
    payload.options.push({ id: o._id, text: o.text });
  });
  try {
    await broker.Publish({
      topic: job.data.contestId,
      payload: {
        type: 'QUESTION',
        data: payload,
      },
    });
    Logger.debug(`Contest Job Publishing to the room: ${job.data.contestId}`);
    return Promise.resolve(job);
  } catch (e) {
    Logger.error(`ERROR => ${e}`);
    throw new Error(`ERROR => ${e}`);
  }
};
