const queues: any[] = [];
export default {
  get: () => queues,
  push: (q: any) => {
    queues.push(q);
  },
};
