import { Request, Response, NextFunction } from 'express';
import ipRangeCheck from 'ip-range-check';
import { configs } from '../config';
export default (req: Request, res: Response, next: NextFunction): void => {
  const ipRanges: string | string[] = configs.app.INTERNAL_IP_RANGES;
  if (ipRanges === '*') {
    next();
  } else {
    if (ipRangeCheck(req.ip, ipRanges)) {
      next();
    } else {
      res
        .status(403)
        .send({ message: 'You cannot access this route', ip: req.ip });
    }
  }
};
