import Redis from 'ioredis';
import * as logger from './Logger';
import { configs } from '../config';
import IORedis = require('ioredis');
let adapter: IORedis.Redis;
const options = {
  port: Number(configs.app.REDIS_PORT),
  host: configs.app.REDIS_HOST,
};
const init = async () => {
  adapter = new Redis(options);
  try {
    logger.debug(`Connecting to redis on ${options.host}:${options.port}`);
    await adapter.ping();
    logger.debug('Connected to redis');
  } catch (e) {
    logger.error('Cannot connect to Redis');
    logger.error(e);
  }
};
export default {
  init,
};
export const getRedisInstance = (): IORedis.Redis => {
  if (adapter) {
    return adapter;
  }
  logger.warn(
    'Redis adapter has not been initialized yet. Trying to connect to redis...',
  );
  adapter = new Redis(options);
  return adapter;
};
