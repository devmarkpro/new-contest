import moment from 'moment';

export const findNearestDate = (dates: Date[], target: Date): number => {
  let nearest = Infinity;
  let winner = -1;
  moment().diff(moment().add(1, 'day'));
  dates.forEach((date: Date, index: number) => {
    const diff = moment(date).diff(moment(target));
    if (diff > 0 && diff < nearest) {
      nearest = diff;
      winner = index;
    }
  });

  return winner;
};
