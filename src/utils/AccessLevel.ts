export default (route: string, access: string): boolean => {
  if (route.indexOf('admin')) {
    return access === 'admin';
  }
  return true;
};
