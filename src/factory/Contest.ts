import factory from 'factory-girl';
// @ts-ignore
module.exports = async () => {
  factory.define(
    'meta_contest',
    {},
    {
      appId: 'moneyket',
      title: 'contest 1-1',
      prize: 500000,
      beginTime: new Date(),
      duration: 100,
      neededCorrectors: 1,
      allowedCorrectorUsageTimes: 1,
      allowCorrectTilQuestion: 10,
      neededTickets: 2,
    },
  );
  factory.define(
    'contest_request',
    {},
    {
      metaId: '5c3fb41a1eec8a364a38cf2b',
      correctAnswers: [1, 1],
      questions: [
        {
          order: 1,
          text: 'test 1',
          options: [
            {
              order: 1,
              text: 'جواب اول',
              hit: 0,
            },
          ],
        },
        {
          order: 2,
          text: 'test 2',
          options: [
            {
              order: 1,
              text: 'aaاب اول',
              hit: 0,
            },
            {
              order: 2,
              text: 'a23112ول',
              hit: 0,
            },
            {
              order: 3,
              text: 'a5ول',
              hit: 0,
            },
            {
              order: 4,
              text: '4 اول',
              hit: 0,
            },
          ],
        },
      ],
    },
  );
  factory.define('questions', {}, [
    {
      order: 1,
      text: 'test 1',
      options: [
        {
          order: 1,
          text: 'جواب اول',
          hit: 0,
        },
      ],
    },
    {
      order: 2,
      text: 'test 2',
      options: [
        {
          order: 1,
          text: 'aaاب اول',
          hit: 0,
        },
        {
          order: 2,
          text: 'a23112ول',
          hit: 0,
        },
        {
          order: 3,
          text: 'a5ول',
          hit: 0,
        },
        {
          order: 4,
          text: '4 اول',
          hit: 0,
        },
      ],
    },
  ]);
};
