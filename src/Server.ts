import dotenv from 'dotenv';
dotenv.config();
// const Sentry = require('@sentry/node');
// const sentry = require('@sentry/core');
import RedisAdapter from './utils/RedisAdapter';
import { debug, middlewareConfig, error } from './utils/Logger';
debug('Initializing env variables...');
import express from 'express';
import bodyParser from 'body-parser';
import { AppErrors } from './utils/Error';
import Mongo from './schemas';
const jwt = require('./utils/jwt');

import { configs } from './config';
import v1 from './controllers/v1';
require('./jobs/ContestJob');
require('./jobs/Queue');
const app = express();
// Sentry.init({ dsn: configs.log.SENTRY_DSN });
// app.use(Sentry.Handlers.requestHandler() as express.RequestHandler);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(jwt());
app.use(middlewareConfig());
const port: number = Number(process.env.PORT);

if (isNaN(port)) {
  throw Error(`'PORT is not defined in env variables ==> ${process.env.PORT}'`);
}

app.use('/v1', v1);
app.get('/', (req: any, res: any) =>
  res.send(
    'Welcome to your new game. There is nothing exists that you can see. Please visit https://www.moneyket.ir',
  ),
);
app.use(
  (
    err: Error,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    error(`API Error: ${err.message}`, {
      body: req.body,
      url: req.url,
      originalUrl: req.originalUrl,
      stack: err.stack,
      date: new Date().toUTCString(),
    });
    if (err instanceof AppErrors) {
      res.status(Number(err.name)).send({
        message: err.message,
      });
    } else {
      // sentry.captureException(err);
      res.status(500).send({
        message:
          configs.app.NODE_ENV === 'production'
            ? AppErrors.UNEXPECTED_ERROR.MSG
            : err.message,
      });
    }
    next(err);
  },
);

Mongo();
RedisAdapter.init();
app.listen(port, () => {
  debug(
    `🌐Server Started... Listening at port :${port}, process: ${[
      process.pid,
    ]} 😍💪`,
  );
});
