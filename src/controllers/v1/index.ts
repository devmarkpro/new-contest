import AdminRoutes from './Admin';
import ContestRoutes from './Public';
import UserRoutes from './User';
import express from 'express';

const router = express.Router();
router.use('/admin', AdminRoutes);
router.use('/contest', ContestRoutes);
router.use('/user', UserRoutes);
export default router;
