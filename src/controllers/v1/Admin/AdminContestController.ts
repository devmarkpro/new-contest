import { BaseController, ResponseModel } from '../../BaseController';
import { ObjectID } from 'mongodb';
import {
  MetaContestDTO,
  AttachRequestDTO,
  UpdateContestDTO,
  AttachQuestionRequestDTO,
} from 'contest';
import * as Responses from 'app-response';
import moment from 'moment-timezone';
import lodash = require('lodash');
import ContestJob from '../../../jobs/ContestJob';
import DeadlineJob from '../../../jobs/DeadlineJob';

export class AdminContestController extends BaseController {
  public AddMeta = async (
    metaData: MetaContestDTO,
  ): Promise<ResponseModel<Responses.AddMetaResponse>> => {
    const metaModel = new this.Models.ContestModel({
      ...metaData,
      questions: [],
      correctAnswers: [],
    });
    const data = await metaModel.save();
    return this.Response({
      id: metaModel._id || '-1',
    });
  };
  public AttachQuestions = async (
    attachRequest: AttachRequestDTO,
  ): Promise<ResponseModel<Responses.AttachQuestionResponse>> => {
    attachRequest.questions = lodash.sortBy(
      attachRequest.questions,
      (a: AttachQuestionRequestDTO) => a.order,
      'asc',
    );
    const meta = await this.Models.ContestModel.findById(attachRequest.metaId);

    if (!meta) {
      throw this.MakeError(
        this.Errors.OBJECT_NOT_FOUND,
        'Contest',
        attachRequest.metaId,
      );
    }
    const n = moment().tz('Asia/Tehran');
    const contestBegin = moment(new Date(meta.beginTime)).tz('Asia/Tehran');
    const contestEnd = moment(new Date(meta.beginTime))
      .tz('Asia/Tehran')
      .add(meta.duration, 'seconds')
      .tz('Asia/Tehran');
    if (n.isSameOrAfter(contestBegin) || n.isSameOrAfter(contestEnd)) {
      throw this.MakeError(this.Errors.CONTEST_IS_PAST);
    }
    if (meta.questions && meta.questions.length > 0) {
      this.Logger.warn(
        'Try to attaching question to the contest which already had question! removing associated jobs....',
      );
      for (const q of meta.questions) {
        const queue = this.CreateJobHandler().Create({
          name: `contest:${q._id.toString()}`,
        });
        await queue.Clean();
        this.Logger.debug(`Job => contest:${q._id.toString()}, Removed`);
      }
    }
    const ids: any[] = [];
    attachRequest.questions.forEach((q: any, questionIndex: number) => {
      q._id = new ObjectID();
      q.options.forEach((a: any, idx: number) => {
        const id = new ObjectID();
        a._id = id;
        if (attachRequest.correctAnswers[questionIndex] - 1 === idx) {
          ids.push(id);
        }
      });
    });
    if (ids.length < attachRequest.questions.length) {
      throw this.MakeError(this.Errors.INCORRECT_CORRECT_ANSWER_INDEX);
    }
    meta.questions = attachRequest.questions as any;
    meta.correctAnswers = ids;
    await meta.save();

    const beginTime = moment(new Date(meta.beginTime)).tz('Asia/Tehran');
    const deadlineModeDelay =
      beginTime.diff(moment().tz('Asia/Tehran')) -
      Number(this.Configs.app.VIDEO_DURATION) * 1000;
    if (
      !beginTime
        .add(deadlineModeDelay, 'milliseconds')
        .isAfter(moment().tz('Asia/Tehran'))
    ) {
      const deadlineQueue = this.CreateJobHandler().Create({
        name: `deadline:${meta._id.toString()}`,
      });
      await deadlineQueue.Clean();
      // deadlineQueue.process(
      //   path.resolve(this.Configs.app.PROJECT_DIR, 'jobs', 'DeadlineJob.ts'),
      // );
      deadlineQueue.AddWorker(DeadlineJob, 5);

      await deadlineQueue.AddDelayedJob(deadlineModeDelay, meta._id.toString());

      this.Logger.debug(
        `Deadline mode, will raise up at ${moment()
          .tz('Asia/Tehran')
          .add(deadlineModeDelay, 'milliseconds')
          .toString()}`,
      );
    }

    const itemDuration = 10;
    let diff = 0;
    for (const [index, question] of meta.questions.entries()) {
      const t = moment(new Date(meta.beginTime)).tz('Asia/Tehran');
      const queue = this.CreateJobHandler().Create({
        name: `contest:${question._id.toString()}`,
      });
      let delay = 0;
      queue.AddWorker(ContestJob, 10);
      if (index === 0) {
        delay = t.diff(moment().tz('Asia/Tehran'));
        diff = delay;
      } else {
        delay = diff + index * (itemDuration * 1000 + 15 * 1000);
      }
      this.Logger.debug(
        `question: ${question.order}, will raise up at ${moment()
          .tz('Asia/Tehran')
          .add(delay, 'milliseconds')
          .toString()}`,
      );
      const data = {
        contestId: meta._id.toString(),
        questionId: question._id.toString(),
        order: question.order,
      };
      await queue.AddDelayedJob(delay, data);
    }

    return this.Response(meta);
  };

  public DeleteContest = async (
    id: string,
  ): Promise<ResponseModel<boolean>> => {
    const contest = await this.Models.ContestModel.findById(id);
    if (!contest) {
      throw this.MakeError(this.Errors.OBJECT_NOT_FOUND, 'Contest', id);
    }
    // @ts-ignore
    await contest.delete();
    return this.Response(true);
  };

  public UpdateContest = async (
    request: UpdateContestDTO,
  ): Promise<ResponseModel<Responses.AttachQuestionResponse>> => {
    const contest = await this.Models.ContestModel.findById(request.meta._id);
    if (!contest) {
      throw this.MakeError(this.Errors.OBJECT_NOT_FOUND, 'Contest', '---');
    }
    contest.beginTime = request.meta.beginTime;
    contest.title = request.meta.title;
    contest.prize = request.meta.prize;
    contest.beginTime = request.meta.beginTime;
    contest.duration = request.meta.duration;
    contest.neededCorrectors = request.meta.neededCorrectors;
    contest.allowedCorrectorUsageTimes =
      request.meta.allowedCorrectorUsageTimes;
    contest.allowCorrectTilQuestion = request.meta.allowCorrectTilQuestion;
    contest.neededTickets = request.meta.neededTickets;
    const ids: any[] = [];
    request.questions.forEach((q: any, questionIndex: number) => {
      q._id = new ObjectID();
      q.options.forEach((a: any, idx: number) => {
        const id = new ObjectID();
        a._id = id;
        if (request.correctAnswers[questionIndex] - 1 === idx) {
          ids.push(id);
        }
      });
    });
    if (ids.length < request.questions.length) {
      throw this.MakeError(this.Errors.INCORRECT_CORRECT_ANSWER_INDEX);
    }
    contest.questions = request.questions as any;
    contest.correctAnswers = ids;
    await contest.save();
    return this.Response(contest);
  };
}
