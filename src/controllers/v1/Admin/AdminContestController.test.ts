import { AdminContestController as ContestController } from './AdminContestController';
import mongoose from 'mongoose';
import { ObjectID } from 'mongodb';
import factory from 'factory-girl';
process.env.TEST_SUITE = 'contest-test';

describe('contest admin', async () => {
  describe('meta contest', async () => {
    test('add meta contest', async () => {
      const contestController: ContestController = new ContestController();
      const meta = await contestController.AddMeta({
        appId: 'moneyket',
        title: 'contest 1-1',
        prize: 500000,
        beginTime: new Date(),
        duration: 100,
        neededCorrectors: 1,
        allowedCorrectorUsageTimes: 1,
        allowCorrectTilQuestion: 10,
        neededTickets: 2,
      });
      expect(meta).toHaveProperty('data');
      expect(ObjectID.isValid(meta.data.id)).toBeTruthy();
    });

    test('attach question to the contest', async () => {
      const contestController: ContestController = new ContestController();
      const meta = await contestController.AddMeta({
        appId: 'moneyket',
        title: 'contest 1-1',
        prize: 500000,
        beginTime: new Date(),
        duration: 100,
        neededCorrectors: 1,
        allowedCorrectorUsageTimes: 1,
        allowCorrectTilQuestion: 10,
        neededTickets: 2,
      });
      expect(meta).toHaveProperty('data');
      expect(ObjectID.isValid(meta.data.id)).toBeTruthy();
      const result = await contestController.AttachQuestions({
        metaId: meta.data.id,
        correctAnswers: [1, 1],
        questions: [
          {
            order: 1,
            text: 'test 1',
            options: [
              {
                order: 1,
                text: 'جواب اول',
                hit: 0,
              },
            ],
          },
          {
            order: 2,
            text: 'test 2',
            options: [
              {
                order: 1,
                text: 'aaاب اول',
                hit: 0,
              },
              {
                order: 2,
                text: 'a23112ول',
                hit: 0,
              },
              {
                order: 3,
                text: 'a5ول',
                hit: 0,
              },
              {
                order: 4,
                text: '4 اول',
                hit: 0,
              },
            ],
          },
        ],
      });
      expect(result.error).toBeNull();
      expect(ObjectID.isValid(result.data._id)).toBeTruthy();
    });
    test('soft delete contest', async () => {
      const contestController: ContestController = new ContestController();

      const meta = await contestController.AddMeta({
        appId: 'moneyket',
        title: 'contest 1-1',
        prize: 500000,
        beginTime: new Date(),
        duration: 100,
        neededCorrectors: 1,
        allowedCorrectorUsageTimes: 1,
        allowCorrectTilQuestion: 10,
        neededTickets: 2,
      });
      const result = await contestController.DeleteContest(meta.data.id);
      expect(result.data).toBeTruthy();
      const metaAfter = await contestController.Models.ContestModel.findById(
        meta.data.id,
      );
      expect(metaAfter).toBeDefined();
      if (metaAfter) {
        expect(metaAfter.deleted).toBeTruthy();
        expect(metaAfter.deletedAt).toBeDefined();
      }
    });
  });
});
