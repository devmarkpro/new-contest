import express from 'express';
import ContestRoutes from './ContestRoutes';
const router = express.Router();

router.use('/', ContestRoutes);
export default router;
