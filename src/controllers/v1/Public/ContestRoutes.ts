import express from 'express';
import { PublicContestController } from './PublicController';
const router = express.Router();
const publicController = new PublicContestController();
router.get(
  '/meta',
  async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const result = await publicController.GetMeta(
        req.user.id,
        req.header('Moneyket-APP'),
      );
      res.status(200).send(result);
    } catch (err) {
      next(err);
    }
  },
);
export default router;
