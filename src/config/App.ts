import path from 'path';
export const {
  NODE_ENV = 'development',
  MONGO_URI = 'mongodb://localhost:27017/contest',
  INTERNAL_IP_RANGES = process.env.INTERNAL_IP_RANGES
    ? process.env.INTERNAL_IP_RANGES.split(',')
    : '*',
  REDIS_HOST = '127.0.0.1',
  REDIS_PORT = 6379,
  EMQ_BASE = 'http://localhost:8080/api/v3',
  EMQ_APP_ID = 'd446b3df8c71',
  EMQ_APP_SECRET = 'Mjg1NTExODAyOTM4ODMzOTQzMzU5OTE5ODE3NTM2MzA3MjA',
  TOPIC_ENCRYPTION_KEY = 'EX3zpk9qMUKDKIy1oQjHZmrn2Vorq8d/X198cND3sg0C9pR1R/Y4aajIW/r6LJ3DmYy21K/kbiKy8RNVsP6mjg==',
  VIDEO_DURATION = 178,
  PROJECT_DIR = path.resolve(__dirname, '..'),
  RABBIT_URL = 'url/?heartbeat=60',
} = process.env;
export const verificationThreshold = 30;
export const QuestionLength = 500;
export const AnswerLength = 200;
export const MinimumQuestionInContest = 3;
export const MinimumAnswerForQuestion = 2;
export const MaximumAnswerForQuestion = 4;
export const MaximumQuestionInContest = 20;
export const PromoteVideoUrl = '';
export const DeadlineSeconds = 160;
