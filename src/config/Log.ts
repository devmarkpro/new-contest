export const LOG_LEVEL = process.env.LOG_LEVEL || 'debug';
export const SENTRY_DSN = process.env.SENTRY_DSN || '';
