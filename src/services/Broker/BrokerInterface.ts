export default interface BrokerInterface<TPublishRequest> {
  Publish(request: TPublishRequest): Promise<boolean>;
  BatchPublish(requests: TPublishRequest[]): Promise<number>;
  GetAllSubs(): Promise<any[]>;
}
