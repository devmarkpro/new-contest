import BrokerInterface from './BrokerInterface';
import { EMQBroker, PublishRequest } from './EMQ/EMQBroker';

export default (): BrokerInterface<PublishRequest> => {
  return new EMQBroker();
};
