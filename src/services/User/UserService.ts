import { debug } from '../../utils/Logger';
export interface UserServiceContract {
  credit: number;
  tickets: number;
  correctors: number;
  referral: {
    url: string;
    title: string;
    message: string;
    dialog: string;
  };
  promoteVideoUrl: string | undefined;
}
export class UserService {
  public id: string | undefined;
  constructor(token?: string) {
    if (token) {
      this.id = token;
    }
  }
  public getUserData = async (): Promise<UserServiceContract> => {
    debug('GetUserData Token: ' + this.id);
    return {
      credit: 100,
      tickets: 100,
      correctors: 100,
      referral: {
        url: 'https://moneyket.ir/download',
        title: '',
        message: '',
        dialog: '',
      },
      promoteVideoUrl: '',
    };
  };
  public isUserValid = async (): Promise<boolean> => {
    debug('isUserValid Token: ', this.id);
    return true;
  };
}
