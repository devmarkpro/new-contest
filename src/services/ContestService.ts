import moment from 'moment-timezone';
import { ContestModel } from '../schemas';
import { ContestMetaModel } from '../models/ContestMetaModel';
import { Types } from 'mongoose';
import { sortBy } from 'lodash';
export const GetTodayContests = async (appId: string) => {
  const today = moment().startOf('day');
  const query = ContestModel.find({
    beginTime: {
      $gte: today.toDate().toISOString(),
      $lte: moment(today)
        .tz('UTC')
        .endOf('day')
        .toDate()
        .toISOString(),
    },
  });
  query
    .and([
      {
        appId: new Types.ObjectId(appId),
      },
    ])
    .sort({ beginTime: 'asc' });
  const contests = query.exec();
  return sortBy(contests, (a: ContestMetaModel) => a.beginTime);
};
export const GetTodayActiveContests = async (appId: string) => {
  const today = moment().startOf('day');
  const query = ContestModel.find({
    beginTime: {
      $gte: moment(),
      $lte: moment(today)
        .endOf('day')
        .toDate()
        .toISOString(),
    },
  });
  query
    .and([
      {
        appId: new Types.ObjectId(appId),
      },
    ])
    .sort({ beginTime: 'asc' });
  return query.exec();
};
