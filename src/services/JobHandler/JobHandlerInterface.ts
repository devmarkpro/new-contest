export default interface JobHandlerInterface<TQueue = any> {
  Create(request: any): JobHandlerInterface;
  Clean(): Promise<void>;
  AddJob(data: any, options?: any): Promise<any>;
  AddDelayedJob(delay: number, data: any, options?: any): Promise<any>;
  AddWorker(job: (jobData: any) => any, count?: number): void;
}
