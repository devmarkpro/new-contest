import JobHandlerInterface from '../JobHandlerInterface';
import Bull, { Queue, Job, JobOptions } from 'bull';
const jobs: any[] = [];
export interface BullRequest {
  name: string;
}
export interface BullJobData {
  data: any;
}
export interface BullJobOptions {
  delay?: number;
}
export class BullJobHandler implements JobHandlerInterface<Queue> {
  public static GetAllJobs() {
    return jobs;
  }
  private queue?: Queue;

  public async AddJob(
    data: BullJobData,
    options?: BullJobOptions,
  ): Promise<any> {
    if (!this.queue) {
      throw new Error('Must be create queue before doing any action');
    }
    return this.queue.add(data, options);
  }

  public async AddDelayedJob(
    delay: number,
    data: BullJobData,
    options?: BullJobOptions,
  ): Promise<Job> {
    if (!this.queue) {
      throw new Error('Must be create queue before doing any action');
    }
    const opts: JobOptions = {
      delay,
      ...options,
    };
    return this.queue.add(data, opts);
  }

  public async AddWorker(
    job: (jobData: any) => any,
    count?: number,
  ): Promise<void> {
    if (!this.queue) {
      throw new Error('Must be create queue before doing any action');
    }
    if (count && count > 0) {
      return this.queue.process(count, job);
    } else {
      return this.queue.process(job);
    }
  }

  public async Clean(): Promise<void> {
    if (!this.queue) {
      throw new Error('Must be create queue before doing any action');
    }
    return this.queue.empty();
  }

  public Create(request: BullRequest): BullJobHandler {
    this.queue = new Bull(request.name);
    jobs.push(this.queue);
    return this;
  }

  // public Create = (request: BullRequest): Queue => {
  //   this.queue =  new Bull(request.name);
  //   return this.queue
  // };
}
