import JobHandlerInterface from './JobHandlerInterface';
import { Queue } from 'bull';
import { BullJobHandler } from './Bull/BullJobHandler';

export default (): JobHandlerInterface<Queue> => {
  return new BullJobHandler();
};
