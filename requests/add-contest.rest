@hostname = http://localhost
@port = 3011
@uri = {{hostname}}:{{port}}
@content_type = application/json
@version = v1
POST {{uri}} HTTP/1.1
content-type: {{{{content_type}}}}

{
    "name": "sample",
    "time": "Wed, 21 Oct 2015 18:27:50 GMT"
}
