const path = require('path');
const nodeExternals = require('webpack-node-externals');

const configs = {
  entry: './src/Server.ts',
  devtool: 'inline-source-map',
  target: 'node',
  mode: process.env.NODE_ENV || 'development',
  externals: [nodeExternals()],
  watch: false,
  watchOptions: {
    ignored: /node_modules|dist/,
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    libraryTarget: 'commonjs2'
  }
};
module.exports = configs;
